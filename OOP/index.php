<?php

    require_once("OOP/animals.php");
    require_once("OOP/frog.php");
    require_once("OOP/ape.php");

    $object = new Animals("sheep");

    
    echo "Nama Hewan : " . $object->name . "<br>";
    echo "Jumlah Kaki : " . $object->legs . "<br>";
    echo "Berdarah Dingin : " . $object->cold_blooded . "<br>";

    echo "<br>";
    $object2 = new Frog("buduk");

    echo "Nama Hewan : " . $object2->name . "<br>";
    echo "Jumlah Kaki: " . $object2-> legs . "<br>";
    echo "Berdarah dingin : " . $object2->cold_blooded . "<br>";
    echo "Jump : " . $object2->jump . "<br>";

    echo "<br>";
    $object3 = new Ape ("kera");

    echo "Nama Hewan : " . $object3->name . "<br>";
    echo "Jumlah Kaki: " . $object3-> legs . "<br>";
    echo "Berdarah dingin : " . $object3->cold_blooded . "<br>";
    echo "Yell : " . $object3->yell . "<br>";

?>